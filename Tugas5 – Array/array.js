// Soal No. 1 (Range)
function range(startNum, finishNum) {
    return rangeWithStep(startNum, finishNum, 1);
}
console.log('Soal No. 1 (Range)')
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    if (!startNum || !finishNum) {
        return -1;
    }

    var resultArray = [];
    if(startNum <= finishNum) {
        var iteration = startNum;
        while(iteration <= finishNum) {
            resultArray.push(iteration);
            iteration += step;
        }
    } else {
        var iteration = startNum;
        while(iteration >= finishNum) {
            resultArray.push(iteration);
            iteration -= step;
        }
    }
    return resultArray;
}

console.log('\nSoal No. 2 (Range with Step)')
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
    if (!startNum && !finishNum && !step) {
        return 0;
    } else if (!finishNum && !step) {
        return startNum;
    } else if (!step) {
        var data = range(startNum, finishNum);    
    } else {
        var data = rangeWithStep(startNum, finishNum, step);
    }
    var result = 0;
    data.forEach(el => {
        result += el;
    });
    return result;
}

console.log('\nSoal No. 3 (Sum of Range)')
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
function dataHandling(data) {
    var result = '';
    data.forEach(dataUser => {
        var tempResult =
            'Nomor ID:  ' + dataUser[0] + '\n' +
            'Nama Lengkap:  ' + dataUser[1] + '\n' +
            'TTL:  ' + dataUser[2] + dataUser[3] + '\n' +
            'Hobi:  ' + dataUser[4];

        if (data.indexOf(dataUser) !== data.length - 1) {
            tempResult += '\n\n';
        }
        result += tempResult;
    });
    return result;
}

console.log('\nSoal No. 4 (Array Multidimensi)')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log(dataHandling(input))

// Soal No. 5 (Balik Kata)

function balikKata(sentence) {
    var result = '';
    for (let i = sentence.length - 1; i >= 0; i --) {
        result += sentence[i];
    }
    return result;
}
console.log('\nSoal No. 5 (Balik Kata)')
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)
function dataHandling2(data) {
    // for modify name variable
    var name = data[1];
    var arrayOfName = name.split(' ');
    arrayOfName.splice(arrayOfName.length, 0, "Elsharawy");
    var newName = arrayOfName.join(' ');
    
    // for modify province variable
    var province = data[2];
    var arrayOfProvince = province.split(' ');
    arrayOfProvince.splice(0, 0, "Provinsi");
    var newProvince = arrayOfProvince.join(' ');

    // for change data value
    data.splice(1, 1, newName);
    data.splice(2, 1, newProvince);
    data.splice(data.length - 1, 1, "Pria", "SMA Internasional Metro");
    
    var ttl = data[3].split('/');
    var ttlToString = ttl.join('-');
    var monthName = convertMonth(ttl[1].slice(1,2));

    ttl.sort(function(a,b) {return b-a});

    console.log(data);
    console.log(monthName);
    console.log(ttl);
    console.log(ttlToString);
    console.log(data[1].slice(0,14));

    return; //because there is no return 
}

// return convert number to month's name 
function convertMonth(monthNumber) {
    let result;
    switch(parseInt(monthNumber)) {
        case 1: result = "Januari"; break;
        case 2: result = "Februari"; break;
        case 3: result = "Maret"; break;
        case 4: result = "April"; break;
        case 5: result = "Mei"; break;
        case 6: result = "Juni"; break;
        case 7: result = "Juli"; break;
        case 8: result = "Agustus"; break;
        case 9: result = "September"; break;
        case 10: result = "Oktober"; break;
        case 11: result = "November"; break;
        case 12: result = "Desember"; break;
    }
    return result;
}

console.log('\nSoal No. 6 (Metode Array)')
var inHadling2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(dataHandling2(inHadling2));