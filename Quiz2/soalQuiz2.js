/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    constructor(email, subject, points) {
      this.email = this.checkEmail(email);
      this.subject = subject;
      if (Array.isArray(points)) {
        this.points = this.average(points);
      } else {
        this.points = points;
      }
    }

    average (points) {
      var sum = 0;
      points.forEach(el => {
        sum += parseInt(el);
      });
      return sum/points.length;
    }

    checkEmail (email) {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        return email
      }
      return 'Email anda tidak valid'
    }
}

a = new Score('irfanazizalamin@gmail.com', 'quiz1', [93,90]);
b = new Score('irfanazizalamin', 'quiz1', [93,90]);
console.log('Nomor 1');
console.log(a)
console.log(b)

/*
  fungsi helper untuk merubah dari array data ke object
*/
function dataToObject(data) {
  var header = data[0];
  var content = data.slice(1,data.length);
  var dataObject = {};

  for(let i = 0; i < header.length; i++) {
    dataObject[header[i]] = [];
    for(let j = 0; j < content.length; j++) {
      dataObject[header[i]].push(content[j][i])
    }
  }
  return dataObject;
}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
*/

function viewScores(data, subject) {
  var dataObject = dataToObject(data);
  var totalStudent = dataObject['email'].length;
  var listOfResult = [];
  
  for(let i = 0; i < totalStudent; i++) {
    let newScore = new Score(
      dataObject['email'][i],
      subject,
      dataObject[subject][i],
    );
    listOfResult.push(newScore);
  }

  console.log(listOfResult);
  return listOfResult;
}

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

console.log('\nNomor 2');
viewScores(data, "quiz - 1");
viewScores(data, "quiz - 2");
viewScores(data, "quiz - 3");

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"
*/
function recapScore(data) {
  var totalStudent = data.length - 1;
  var listOfStudent = [];

  for(let i = 1; i < totalStudent + 1; i++) {
    let newScore = new Score(
      data[i][0],
      'final - score',
      data[i].slice(1,data.length)
    )
    listOfStudent.push(newScore)
  }

  console.log('output:')
  for(let i = 0; i < totalStudent; i++) {
    if(i + 1 === totalStudent) lineBrake = ""
    else lineBrake = lineBrake = "\n"

    let points = listOfStudent[i].points;
    console.log(`${i+1}. Email: ${listOfStudent[i].email}`);
    console.log(`Rata-rata: ${points}`);
    console.log(`Predikat: ${checkPredicate(points)}${lineBrake}`);
  }
}

/**
  fugsi helper untuk melakukan check predikat 
  berdasarkan final score dari student  
*/
function checkPredicate(score) {
  var result = "";
  if (score > 70) {result = "participant"}
  if (score > 80) {result = "graduate"}
  if (score > 90) {result = "honour"}
  return result;
}

console.log('\nNomor 3');
recapScore(data);