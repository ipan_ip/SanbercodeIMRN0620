// No. 1 Looping While
console.log('No. 1 Looping While')

var iteration = 0;
var number = 0;
while (iteration < 22) {
    if (iteration < 11) {
        if (number === 0) {
            console.log('LOOPING PERTAMA')
        } else {
            console.log(`${number} - I love coding`)
        }
        number += 2;
    } else {
        if (number === 22) {
            console.log('LOOPING KEDUA')
        } else {
            console.log(`${number} - I will become a mobile developer`)
        }
        number -= 2;
    }
    iteration++;
}

// No. 2 Looping menggunakan for
console.log('\nNo. 2 Looping menggunakan for')

for(var i = 1; i <= 20; i++) {
    if (i % 2 === 0) {
        console.log(`${i} - Berkualitas`);
    } else {
        if(i % 3 === 0) {
            console.log(`${i} - I Love Coding`);
        } else {
            console.log(`${i} - Santai`);
        }
    }
}

// No. 3 Membuat Persegi Panjang
console.log('\nNo. 3 Membuat Persegi Panjang')

for(var i = 0; i < 4; i++) {
    let rowNumber3 = '';
    for(var j = 0; j < 8; j++) {
        rowNumber3 += '#';
    }
    console.log(`${rowNumber3}`);
}

// No. 4 Membuat Tangga
console.log('\nNo. 4 Membuat Tangga')

for(var i = 0; i < 7; i++) {
    let rowNumber4 = '';
    for(var j = 0; j < i+1; j++) {
        rowNumber4 += '#';
    }
    console.log(`${rowNumber4}`);
}

// No. 5 Membuat Papan Catur
console.log('\nNo. 5 Membuat Papan Catur')

for(var i = 0; i < 8; i++) {
    let rowNumber5 = '';
    for(var j = 0; j < 8; j++) {
        if (i % 2 === 0){
            if (j % 2 === 0) {
                rowNumber5 += ' ';
            } else {
                rowNumber5 += '#';
            }
        } else {
            if (j % 2 === 0) {
                rowNumber5 += '#';
            } else {
                rowNumber5 += ' ';
            }
        }
    }
    console.log(`${rowNumber5}`);
}