function descendingTen(num) {
    if (!num) return -1;
    else {
        var index = num;
        var result = '';
        while (index > num - 10) {
            result = result + index + ' ';
            index--;
        }
        return result;
    }
}

function ascendingTen(num) {
    if (!num) return -1;
    else {
        var index = num;
        var result = '';
        while (index < num + 10) {
            result = result + index + ' ';
            index++;
        }
        return result;
    }
}

function conditionalAscDesc(ref, check) {
    if (!ref || !check) return -1;

    if (check % 2 === 0) {
        return descendingTen(ref);
    } else {
        return ascendingTen(ref);
    }
}

function ularTangga() {
    var index = 10;
    var result = '';
    while (index > 0) {
        if (index % 2 === 0) {
            result = result + descendingTen(index*10) + '\n';
        } else {
            result = result + ascendingTen(((index-1)*10) + 1) + '\n';
        }
        index--;
    }
    return result;
}

// TEST CASES Descending Ten
console.log(descendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(descendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(descendingTen()) // -1

// TEST CASES Ascending Ten
console.log(ascendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(ascendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(ascendingTen()) // -1

// TEST CASES Conditional Ascending Descending
console.log(conditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(conditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(conditionalAscDesc(31)) // -1
console.log(conditionalAscDesc()) // -1

// TEST CASE Ular Tangga
console.log(ularTangga())
/*
Output :
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/