function bandingkan(num1, num2) {
    var errorCondition1 = !num1 || !num2;
    var errorCondition2 = num1 === num2;
    var errorCondition3 = num1 < 0 || num2 < 0;
    if (errorCondition1 || errorCondition2 || errorCondition3) {
        return -1;
    } else {
        return Math.max(num1, num2);
    }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

function balikString(inputString) {
    var result = "";
    for (var i = inputString.length-1; i >= 0; i--) {
        result += inputString[i];
    }
    return result;
}

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

function palindrome(inputString) {
    if (inputString.length <= 1) {
        return true;
    } else {
        var cek = true;
        var endIndex = inputString.length - 1;
        var currentIndex = 0;
        while (currentIndex < Math.ceil(inputString.length / 2)) {
            if (cek === false) {
                return false;
            } else {
                if (inputString[currentIndex] === inputString[endIndex]) {
                    endIndex--;
                } else {
                    return false;
                }
            }
            currentIndex++;
        }
        return true;
    }
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false