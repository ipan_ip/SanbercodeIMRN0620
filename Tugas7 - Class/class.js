// 1. Animal Class
// Release 0
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
    get animalName () {
        return this.name;
    }
    set animalName (animalName)  {
        this.name = animalName;
    }
    get animalLegs () {
        return this.legs;
    }
    set animalLegs (animalLegs) {
        this.legs = animalLegs;
    }
    get animalBlood () {
        return this.cold_blooded;
    }
    set animalBlood (bool) {
        this.cold_blooded = bool;
    }
}

// Release 1
class Ape extends Animal {
    constructor(name, legs = 2) {
        super(name, legs);
    }
    yell () {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name, legs = 2) {
        super(name, legs);
    }
    jump () {
        console.log("hop hop");
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// 2. Function to Class
class Clock {
    constructor({template}) {
        this.template=template;
        this.timer;
    }

    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    start () {
        this.timer = setInterval(this.render.bind(this), 1000);
    };
}

var clock = new Clock({template: 'h:m:s'});
clock.start();
