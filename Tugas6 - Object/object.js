function arrayToObject(arr) {
    const thisYear = new Date().getFullYear();
    var obj = {};
    arr.forEach(el => {
        var insideObj = {};
        insideObj.firstName = el[0];
        insideObj.lastName = el[1];
        insideObj.gender = el[2];
        if (el[3] > thisYear || !el[3]) insideObj.age = "Invalid birth year";
        else insideObj.age = el[3];
        obj[el[0] + ' ' + el[1]] = insideObj;
    });
    console.log(obj);
}

// Driver Code 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

function shoppingTime(memberId, money) {
    var listItems = {
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N' : 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone' : 50000
    };
    var listPurchased = [];
    var total = 0;
    var currentMoney = money;
    var lowerLimit = 50000;

    if (!memberId || memberId === '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < lowerLimit) {
        return 'Mohon maaf, uang tidak cukup';
    }

    Object.entries(listItems).map(([key, val]) => {
        if (currentMoney >= val) {
            listPurchased.push(key);
            currentMoney -= val;
            total += val;
        }
    })
    return {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: money - total
    };
}

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var pricePerRoute = 20000;
    var result = [];

    arrPenumpang.forEach(el => {
        var name = el[0];
        var start = el[1];
        var finish = el[2];
        var distance = Math.abs(rute.indexOf(start) - rute.indexOf(finish));
        var price = distance * pricePerRoute;
        result.push({
            'penumpang': name,
            'naikDari': start,
            'tujuan': finish,
            'bayar': price
        });
    });

    return result;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));