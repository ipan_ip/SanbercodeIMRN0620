// Soal 1

var nama = "s";
var peran = "Penyihssir";

let errorMessage = ""

if (nama === '' || peran === '') {
    if (nama === '' && peran === '') {
        errorMessage = "Nama dan peran harus diisi";
    } else if (nama === '') {
        errorMessage = "Nama harus diisi!";
    } else if (peran === '') {
        errorMessage = `Halo ${nama}, Pilih peranmu untuk memulai game!`;
    }
    console.log(errorMessage)
} else {
    const message = 'Selamat datang di Dunia Werewolf, ';
    switch(peran) {
        case 'Penyihir': {
            console.log(`${message} ${peran} \nHalo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
            break
        }
        case 'Guard': {
            console.log(`${message} ${peran} \nHalo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
            break
        }
        case 'Werewolf': {
            console.log(`${message} ${peran} \nHalo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
        }
        default: {
            console.log("Peran tidak tersedia")
        }
    }   
}

// Soal 2
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

let bulanString;
switch(bulan) {
    case 1: bulanString = "Januari"; break;
    case 2: bulanString = "Februari"; break;
    case 3: bulanString = "Maret"; break;
    case 4: bulanString = "April"; break;
    case 5: bulanString = "Mei"; break;
    case 6: bulanString = "Juni"; break;
    case 7: bulanString = "Juli"; break;
    case 8: bulanString = "Agustus"; break;
    case 9: bulanString = "September"; break;
    case 10: bulanString = "Oktober"; break;
    case 11: bulanString = "November"; break;
    case 12: bulanString = "Desember"; break;
}

console.log(`${hari} ${bulanString} ${tahun}`)