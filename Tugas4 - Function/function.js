// Funtion No. 1
function teriak() {
    return('Halo Sanbers!');
}

// Funtion No. 2
function kalikan(a, b) {
    return a*b;
}

// Funtion No. 3
function introduce(name, age, address, hobby) {
    return(`Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`);
}

function runAllNumber() {
    // No. 1
    console.log('No. 1');
    console.log(teriak());

    // No. 2
    console.log('\nNo. 2');
    var num1 = 12;
    var num2 = 4;
    var hasilKali = kalikan(num1, num2);
    console.log(hasilKali);

    // No. 3
    console.log('\nNo. 3');
    var name = 'Irfan';
    var age = 21;
    var address = 'Boyolali, Jawa Tengah';
    var hobby = 'Ngoding haha :)';
    console.log(introduce(name, age, address, hobby));
}

runAllNumber();