// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 9000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function checkReadBooks (books) {
    readBooks(10000, books[0], function(resultFirstBook) {
        readBooks(resultFirstBook, books[1], function(resultSecondBook) {
            readBooks(resultSecondBook, books[2], function(resultSecondBook) {
                return;
            })
        }) 
    })
}

checkReadBooks(books);