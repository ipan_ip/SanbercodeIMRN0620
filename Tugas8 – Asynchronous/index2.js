var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise Versi
function checkReadBooks2 (books) {
    readBooksPromise(10000, books[0])
        .then(time1 => {
            return readBooksPromise(time1, books[1])
        })
        .then(time2 => {
            return readBooksPromise(time2, books[2])
        })
        .catch(function(error) {
            console.log(error);
        })
}

checkReadBooks2(books);